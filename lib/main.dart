import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tmdb_hot_movies_demo/BLoC/Search/search_bloc.dart';
import 'package:tmdb_hot_movies_demo/BLoC/Movie/movie_bloc.dart';
import 'package:tmdb_hot_movies_demo/BLoC/Movie/movie_event.dart';
import 'package:tmdb_hot_movies_demo/BLoC/Movie/movie_state.dart';
import 'package:tmdb_hot_movies_demo/BLoC/Video/video_bloc.dart';
import 'package:tmdb_hot_movies_demo/Utils/key.dart';
import 'package:tmdb_hot_movies_demo/Utils/movie_repositories.dart';
import 'package:tmdb_hot_movies_demo/View/movie_list_page.dart';
import 'package:tmdb_hot_movies_demo/View/error_page.dart';
import 'package:tmdb_hot_movies_demo/View/movie_search_page.dart';
import 'package:shimmer/shimmer.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: launchTitle,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LaunchPage(),
    );
  }
}

class LaunchPage extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<LaunchPage> {
  @override
  void initState() {
    super.initState();

    Future.delayed(Duration(seconds: 3), () async {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => AppPortal()));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Center(
        child: Hero(
          tag:launchTitle,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                launchTitle,
                style: TextStyle(
                    color: Colors.redAccent,
                    fontSize: 50,
                    fontWeight: FontWeight.w600),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class AppPortal extends StatefulWidget {
  @override
  _AppPortalState createState() => _AppPortalState();
}

class _AppPortalState extends State<AppPortal> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    return MultiBlocProvider(
      providers: [
        BlocProvider<MovieBloc>(
          create: (context) => MovieBloc(repository: MovieRepositoryImpl()),
        ),
        BlocProvider<SearchMovieBloc>(
          create: (context) =>
              SearchMovieBloc(repository: MovieRepositoryImpl()),
        ),
        BlocProvider<VideoBloc>(
          create: (context) => VideoBloc(repository: MovieRepositoryImpl()),
        ),
      ],
      child: MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primarySwatch: Colors.red,
          ),
          home: AnnotatedRegion<SystemUiOverlayStyle>(
            value: SystemUiOverlayStyle(
                statusBarColor: Colors.transparent,
                statusBarIconBrightness: Brightness.light),
            child: Scaffold(backgroundColor: Colors.black, body: HomePage()),
          )),
    );
  }
}


class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int activeIndex = 0;

  MovieBloc movieBloc;

  @override
  void initState() {
    super.initState();
    movieBloc = BlocProvider.of<MovieBloc>(context);
    movieBloc.add(FetchMovieEvent(movieType: MovieCategory.keys.first));
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Material(
      type: MaterialType.transparency,
      child: SafeArea(
        bottom: false,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              appBar(width),
              tabBar(),
              SizedBox(
                height: 10,
              ),
              Expanded(
                child:
                    BlocBuilder<MovieBloc, MovieState>(builder: (context, state) {
                  if (state is MovieInitialState) {
                    return movieListLoadingView();
                  } else if (state is MovieLoadingState) {
                    return movieListLoadingView();
                  } else if (state is MovieLoadedState) {
                    return MovieListView(state.movies);
                  } else if (state is MovieErrorState) {
                    return DefaultErrorView();
                  }
                  return DefaultErrorView();
                }),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget appBar(width) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Hero(
          tag:launchTitle,
          child: 
            Row(
              children: <Widget>[
                Text(
                  launchTitle,
                  style: TextStyle(
                      decoration: TextDecoration.none,
                      color: Colors.white,
                      fontSize: width * 0.1,
                      fontWeight: FontWeight.w600),
                ),
              ],
            ),
        ),
        IconButton(
          icon: Icon(Icons.search),
          color: Colors.white,
          iconSize: width * 0.1,
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (BuildContext context) {
              return SearchPage();
            }));
          },
        )
      ],
    );
  }

  Widget tabBar() {
    var keys = MovieCategory.keys.toList();
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          for (int i = 0; i < keys.length; i++)
            tabItem(i, MovieCategory[keys[i]], keys[i]),
        ],
      ),
    );
  }

  Widget tabItem(int index, String title, String movieType) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 4.0, vertical: 16),
      child: InkWell(
        onTap: () {
          movieBloc.add(FetchMovieEvent(movieType: movieType));
          setState(() {
            activeIndex = index;
          });
        },
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              color:
                  activeIndex == index ? Colors.redAccent : Color(0xFF303030)),
          child: Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 8.0, horizontal: 18.0),
            child: Text(
              title,
              style: TextStyle(
                fontSize: MediaQuery.of(context).size.width * 0.035,
                letterSpacing: 1,
                fontWeight: FontWeight.w600,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),
    );
  }
}

/// 載入動畫
Widget movieListLoadingView() {
  int offset = 0;
  int time;
  return ListView.builder(
      itemCount: 4,
      itemBuilder: (BuildContext context, int index) {
        offset += 10;
        time = 800 + offset;
        return Shimmer.fromColors(
          period: Duration(milliseconds: time),
          highlightColor: Color(0xFF1a1c20),
          baseColor: Color(0xFF111111),
          child: Container(
            margin: EdgeInsets.all(4.0),
            height: MediaQuery.of(context).size.height * 0.2,
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Positioned(
                  left: 16,
                  top: 0,
                  height: MediaQuery.of(context).size.height * 0.2,
                  width: MediaQuery.of(context).size.width,
                  child: Container(
                    width: 145,
                    alignment: Alignment.centerRight,
                    decoration: BoxDecoration(
                        color: Color(0xFF333333),
                        borderRadius: BorderRadius.circular(12)),
                  ),
                ),
              ],
            ),
          ),
        );
      });
}