// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api_video_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VideoResults _$VideoResultsFromJson(Map<String, dynamic> json) {
  return VideoResults(
      id: json['id'] as int,
      results: (json['results'] as List)
          ?.map((e) =>
              e == null ? null : VideoItem.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$VideoResultsToJson(VideoResults instance) =>
    <String, dynamic>{'id': instance.id, 'results': instance.results};

VideoItem _$VideoItemFromJson(Map<String, dynamic> json) {
  return VideoItem(
      id: json['id'] as String,
      iso6391: json['iso_639_1'] as String,
      iso31661: json['iso_3166_1'] as String,
      key: json['key'] as String,
      name: json['name'] as String,
      site: json['site'] as String,
      size: json['size'] as int,
      type: json['type'] as String);
}

Map<String, dynamic> _$VideoItemToJson(VideoItem instance) => <String, dynamic>{
      'id': instance.id,
      'iso_639_1': instance.iso6391,
      'iso_3166_1': instance.iso31661,
      'key': instance.key,
      'name': instance.name,
      'site': instance.site,
      'size': instance.size,
      'type': instance.type
    };
