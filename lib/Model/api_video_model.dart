import 'package:json_annotation/json_annotation.dart';

part 'api_video_model.g.dart';

@JsonSerializable()
class VideoResults {
  int id;
  List<VideoItem> results;

  VideoResults({
    this.id,
    this.results,
  });

  factory VideoResults.fromJson(Map<String, dynamic> json) => _$VideoResultsFromJson(json);
  Map<String, dynamic> toJson() => _$VideoResultsToJson(this);
}

@JsonSerializable()
class VideoItem {
  String id;
  @JsonKey(name: 'iso_639_1')
  String iso6391;
  @JsonKey(name: 'iso_3166_1')
  String iso31661;
  String key;
  String name;
  String site;
  int size;
  String type;

  VideoItem({
    this.id,
    this.iso6391,
    this.iso31661,
    this.key,
    this.name,
    this.site,
    this.size,
    this.type,
  });

  factory VideoItem.fromJson(Map<String, dynamic> json) => _$VideoItemFromJson(json);
  Map<String, dynamic> toJson() => _$VideoItemToJson(this);
}