import 'package:json_annotation/json_annotation.dart';

part 'api_result_model.g.dart';

@JsonSerializable()
class ApiResultModel {
  int page;
  @JsonKey(name: 'total_results')
  int totalResults;
  @JsonKey(name: 'total_pages')
  int totalPages;
  List<Results> results;

  ApiResultModel({this.page, this.totalResults, this.totalPages, this.results});

  factory ApiResultModel.fromJson(Map<String, dynamic> json) => _$ApiResultModelFromJson(json);
  Map<String, dynamic> toJson() => _$ApiResultModelToJson(this);
}

@JsonSerializable()
class Results {
  double popularity;
  @JsonKey(name: 'vote_count')
  int voteCount;
  bool video;
  @JsonKey(name: 'poster_path')
  String posterPath;
  int id;
  bool adult;
  @JsonKey(name: 'backdrop_path')
  String backdropPath;
  @JsonKey(name: 'original_language')
  String originalLanguage;
  @JsonKey(name: 'original_title')
  String originalTitle;
  @JsonKey(name: 'genre_ids')
  List<int> genreIds;
  String title;
  @JsonKey(name: 'vote_average')
  dynamic voteAverage;
  String overview;
  @JsonKey(name: 'release_date')
  String releaseDate;

  Results(
      {this.popularity,
      this.voteCount,
      this.video,
      this.posterPath,
      this.id,
      this.adult,
      this.backdropPath,
      this.originalLanguage,
      this.originalTitle,
      this.genreIds,
      this.title,
      this.voteAverage,
      this.overview,
      this.releaseDate});

  factory Results.fromJson(Map<String, dynamic> json) => _$ResultsFromJson(json);
  Map<String, dynamic> toJson() => _$ResultsToJson(this);
}

@JsonSerializable()
class Genres {
  int id;
  String name;

  Genres({this.id, this.name});

  factory Genres.fromJson(Map<String, dynamic> json) => _$GenresFromJson(json);
  Map<String, dynamic> toJson() => _$GenresToJson(this);
}
