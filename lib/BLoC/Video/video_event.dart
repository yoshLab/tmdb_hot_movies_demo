import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

class VideoEvent extends Equatable {
  List<Object> get props => [];
}

class FetchVideoEvent extends VideoEvent {
  final String movieId;
  FetchVideoEvent({@required this.movieId});
  
  @override
  List<Object> get props => [movieId];
  get moviesId => movieId;
}