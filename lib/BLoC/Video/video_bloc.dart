import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:tmdb_hot_movies_demo/BLoC/Video/video_state.dart';
import 'package:tmdb_hot_movies_demo/BLoC/Video/video_event.dart';
import 'package:tmdb_hot_movies_demo/Model/api_video_model.dart';
import 'package:tmdb_hot_movies_demo/Utils/movie_repositories.dart';

class VideoBloc extends Bloc<VideoEvent, VideoState> {
  MovieRepository repository;

  VideoBloc({@required this.repository});

  @override
  VideoState get initialState => VideoInitialState();

  @override
  Stream<VideoState> mapEventToState(VideoEvent event) async* {
    if (event is FetchVideoEvent) {
      yield VideoLoadingState();
      try {
        List<VideoItem> videos = await repository.getVideos(event.movieId);
        yield VideoLoadedState(videos: videos);
      } catch (e) {
        yield VideoErrorState(message: e.toString());
      }
    }
  }
}
