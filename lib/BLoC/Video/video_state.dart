import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:tmdb_hot_movies_demo/Model/api_video_model.dart';

abstract class VideoState extends Equatable {
  List<Object> get props => [];
}

class VideoInitialState extends VideoState {
}

class VideoLoadingState extends VideoState {
}

class VideoLoadedState extends VideoState {
  List<VideoItem> videos;
  VideoLoadedState({@required this.videos});
  get videosList => videos;
}

class VideoErrorState extends VideoState {
  String message;
  VideoErrorState({@required this.message});
  get errorMessage => message;
}
