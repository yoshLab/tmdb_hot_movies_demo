import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

abstract class MovieEvent extends Equatable {
  List<Object> get props => [];
}

class FetchMovieEvent extends MovieEvent {
  final String movieType;
  FetchMovieEvent({@required this.movieType});
  
  @override
  List<Object> get props => [movieType];
  get moviesType => movieType;
}