import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:tmdb_hot_movies_demo/Model/api_result_model.dart';

abstract class MovieState extends Equatable {
  List<Object> get props => [];
}

class MovieInitialState extends MovieState {
}

class MovieLoadingState extends MovieState {
}

class MovieLoadedState extends MovieState {
  List<Results> movies;
  MovieLoadedState({@required this.movies});
  get moviesList => movies;
}

class MovieErrorState extends MovieState {
  String message;
  MovieErrorState({@required this.message});
  get errorMessage => message;
}
