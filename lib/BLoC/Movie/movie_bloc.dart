import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:tmdb_hot_movies_demo/BLoC/Movie/movie_state.dart';
import 'package:tmdb_hot_movies_demo/BLoC/Movie/movie_event.dart';
import 'package:tmdb_hot_movies_demo/Model/api_result_model.dart';
import 'package:tmdb_hot_movies_demo/Utils/movie_repositories.dart';

class MovieBloc extends Bloc<MovieEvent, MovieState> {
  MovieRepository repository;

  MovieBloc({@required this.repository});

  @override
  MovieState get initialState => MovieInitialState();

  @override
  Stream<MovieState> mapEventToState(MovieEvent event) async* {
    if (event is FetchMovieEvent) {
      yield MovieLoadingState();
      try {
        List<Results> movies = await repository.getMovies(event.movieType);
        yield MovieLoadedState(movies: movies);
      } catch (e) {
        yield MovieErrorState(message: e.toString());
      }
    }
  }
}
