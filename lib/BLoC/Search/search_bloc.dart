import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:tmdb_hot_movies_demo/BLoC/Search/search_event.dart';
import 'package:tmdb_hot_movies_demo/BLoC/Search/search_state.dart';
import 'package:tmdb_hot_movies_demo/Model/api_result_model.dart';
import 'package:tmdb_hot_movies_demo/Utils/movie_repositories.dart';

class SearchMovieBloc extends Bloc<SearchEvent, SearchMovieState> {
  MovieRepository repository;

  SearchMovieBloc({@required this.repository});

  @override
  SearchMovieState get initialState => SearchMovieInitialState();

  @override
  Stream<SearchMovieState> mapEventToState(SearchEvent event) async* {
    if (event is FetchMovieBySearchEvent) {
      yield SearchMovieLoadingState();
      try {
        List<Results> movies = await repository.getMoviesBySearch(event.query);
        yield SearchMovieLoadedState(movies: movies);
      } catch (e) {
        yield SearchMovieErrorState(message: e.toString());
      }
    }
    else if (event is CloseSearchEvent) {
      yield SearchMovieEndedState();
    }
  }
}
