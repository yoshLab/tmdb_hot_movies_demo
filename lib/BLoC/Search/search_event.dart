import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

abstract class SearchEvent extends Equatable {
  List<Object> get props => [];
}

class FetchMovieBySearchEvent extends SearchEvent {
  final String query;
  FetchMovieBySearchEvent({@required this.query});

  @override
  List<Object> get props => [query];
}

class CloseSearchEvent extends SearchEvent {
}
