import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:tmdb_hot_movies_demo/Model/api_result_model.dart';

abstract class SearchMovieState extends Equatable {
  List<Object> get props => [];
}

class SearchMovieInitialState extends SearchMovieState {
}

class SearchMovieLoadingState extends SearchMovieState {
}

class SearchMovieLoadedState extends SearchMovieState {
  final List<Results> movies;
  SearchMovieLoadedState({@required this.movies});
  get moviesList => movies;
}

class SearchMovieErrorState extends SearchMovieState {
  final String message;
  SearchMovieErrorState({@required this.message});
}

class SearchMovieEndedState extends SearchMovieState {
}