import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:tmdb_hot_movies_demo/Model/api_result_model.dart';
import 'package:tmdb_hot_movies_demo/View/movie_detail_page.dart';
import 'package:tmdb_hot_movies_demo/View/error_page.dart';
import 'package:tmdb_hot_movies_demo/Utils/key.dart';

class MovieListView extends StatelessWidget {
  final List<Results> movies;
  MovieListView(this.movies);

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    if (movies == null || movies?.length == 0)
      return EmptyItemView();

    return ListView.separated(
        shrinkWrap: true,
        separatorBuilder: (context, index) => Divider(
              color: Colors.transparent,
            ),
        itemCount: movies.length,
        itemBuilder: (BuildContext context, int index) {
          final List<String> geners = getMovieGenres(movies[index].genreIds);

          return InkWell(
            child: 
                Container(
                  margin: EdgeInsets.all(4.0),
                  height: height * 0.2,
                  child: Stack(
                    alignment: Alignment.center,
                    children: <Widget>[
                      Positioned(
                        child: Container(
                          height: height,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  begin: Alignment.topRight,
                                  end: Alignment.bottomLeft,
                                  colors: [
                                    Color(0xFF202020),
                                    Colors.black,
                                  ]),
                              borderRadius: BorderRadius.circular(12)
                              ),
                          child: Row(
                            children: <Widget>[
                              SizedBox(
                                width: height * 0.16 + 16,
                              ),
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 8.0, horizontal: 16),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        movies[index].title,
                                        maxLines: 2,
                                        overflow: TextOverflow.fade,
                                        style: TextStyle(
                                            color: Colors.white,
                                            // color: Colors.blueGrey,
                                            fontWeight: FontWeight.w200,
                                            fontSize:
                                                MediaQuery.of(context).size.width *
                                                    0.055),
                                      ),
                                      movieGenersList(context, geners),
                                      Row(
                                        children: <Widget>[
                                          Text(
                                            "⭐  ${movies[index].voteAverage}",
                                            style: TextStyle(
                                                color: Colors.yellowAccent,
                                                fontWeight: FontWeight.w200,
                                                fontSize: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.033),
                                          ),
                                          SizedBox(
                                            child: Text(
                                              "  | ",
                                              style: TextStyle(
                                                  color: Colors.white54,
                                                  fontWeight: FontWeight.w100,
                                                  fontSize: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.033),
                                            ),
                                          ),
                                          Text(
                                            " ${movies[index].releaseDate}",
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontWeight: FontWeight.w200,
                                                fontSize: MediaQuery.of(context)
                                                        .size.width * 0.033),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Positioned(
                        left: 16,
                        top: 0,
                        height: height * 0.2,
                        width: height * 0.16,
                        child: Hero(
                          transitionOnUserGestures: true,
                          tag: "${movies[index].id}",
                          child: Container(
                            alignment: Alignment.centerRight,
                            decoration: BoxDecoration(
                                color: Color(0xFF333333),
                                borderRadius: BorderRadius.circular(12)
                            ),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(12),
                              child: kIsWeb
                                ? Image.network(
                                    baseImageUrl + "${movies[index].posterPath}",
                                    width: 145,
                                    fit: BoxFit.fitWidth)
                                : CachedNetworkImage(
                                    width: 145,
                                    imageUrl:
                                        baseImageUrl + "${movies[index].posterPath}",
                                    fit: BoxFit.fitWidth,
                                  ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),  
                ),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) =>
                      Details(movies[index])));
            },                     
          );
        });
  }
}

Widget movieGenersList(context, List<String> strings) {
  List<Widget> list = new List<Widget>();
  for (var i = 0; i < strings.length; i++) {
    list.add(new Text(
      strings[i],
      style: TextStyle(
          color: Colors.white60,
          fontSize: MediaQuery.of(context).size.width * 0.033),
    ));
  }
  return new Wrap(runSpacing: 4.0, spacing: 10.0, children: list);
}
