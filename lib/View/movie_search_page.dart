import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tmdb_hot_movies_demo/BLoC/Search/search_bloc.dart';
import 'package:tmdb_hot_movies_demo/BLoC/Search/search_event.dart';
import 'package:tmdb_hot_movies_demo/BLoC/Search/search_state.dart';
import 'package:tmdb_hot_movies_demo/Model/api_result_model.dart';
import 'package:tmdb_hot_movies_demo/View/movie_list_page.dart';
import 'package:tmdb_hot_movies_demo/View/error_page.dart';
import 'package:tmdb_hot_movies_demo/main.dart';

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  TextEditingController textEditingController;
  bool filter = false;
  FocusNode focusNode;
  SearchMovieBloc searchMovieBloc;
  List<Results> moviesList = [];
  @override
  void initState() {
    textEditingController = TextEditingController();
    focusNode = FocusNode();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: false,
      child: Scaffold(
        backgroundColor: Colors.black,
        body: InkWell(
          splashColor: Colors.transparent,
          onTap: () {
            focusNode.unfocus();
          },
          child: Column(
            children: <Widget>[
              searchBox(),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: BlocBuilder<SearchMovieBloc, SearchMovieState>(
                      builder: (context, state) {
                    if (state is SearchMovieInitialState || state is SearchMovieEndedState) {
                      return EmptyItemView(emptyTitle: "開始搜尋吧！", emptyMessage: "",);
                    } 
                    else if (state is SearchMovieLoadingState) {
                      return movieListLoadingView();
                    } 
                    else if (state is SearchMovieLoadedState) {
                      // if (state.movies.length == 0) {
                      //   return Center(child: EmptyItemView());
                      // }
                      return MovieListView(state.movies);
                    } else if (state is SearchMovieErrorState) {
                      return DefaultErrorView();
                    }
                    return DefaultErrorView();
                  }),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget searchBox() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            IconButton(
              icon: Icon(
                Icons.arrow_back,
                size: MediaQuery.of(context).size.width * 0.08,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.pop(context);
                setState(() {
                    searchMovieBloc =
                        BlocProvider.of<SearchMovieBloc>(context);
                    searchMovieBloc.add(CloseSearchEvent());
                  });
              },
            ),
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: Color(0xFF222222),
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 16),
                  child: TextFormField(
                    controller: textEditingController,
                    autofocus: true,
                    focusNode: focusNode,
                    textInputAction: TextInputAction.search,
                    onFieldSubmitted: (value) {
                      setState(() {
                        searchMovieBloc =
                            BlocProvider.of<SearchMovieBloc>(context);
                        searchMovieBloc.add(FetchMovieBySearchEvent(
                            query: textEditingController.text));
                      });
                    },
                    cursorColor: Color(0xFFdddddd),
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.white),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: "搜尋",
                      hintStyle: TextStyle(
                        fontSize: 18,
                        color: Colors.white54,
                      ),
                      suffixIcon: InkWell(
                        child: Icon(
                          Icons.close,
                          size: 23,
                          color: Color(0xFF888888),
                        ),
                        onTap: () {
                          setState(() {
                            textEditingController.clear();
                          });
                        },
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}


