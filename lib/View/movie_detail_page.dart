import 'package:cached_network_image/cached_network_image.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tmdb_hot_movies_demo/Model/api_video_model.dart';
import 'package:tmdb_hot_movies_demo/BLoC/Video/video_bloc.dart';
import 'package:tmdb_hot_movies_demo/BLoC/Video/video_event.dart';
import 'package:tmdb_hot_movies_demo/BLoC/Video/video_state.dart';
import 'package:tmdb_hot_movies_demo/Model/api_result_model.dart';
import 'package:tmdb_hot_movies_demo/View/error_page.dart';
import 'package:tmdb_hot_movies_demo/Utils/key.dart';

class Details extends StatefulWidget {
  final Results movies;

  Details(this.movies);

  @override
  _DetailsState createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  VideoBloc videoBloc;

  @override
  void initState() {
    super.initState();
    videoBloc = BlocProvider.of<VideoBloc>(context);
    videoBloc.add(FetchVideoEvent(movieId: "${widget.movies.id}"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Stack(
            alignment: Alignment.bottomLeft,
            children: <Widget>[
              Hero(
                transitionOnUserGestures: true,
                tag: "${widget.movies.id}",
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.57,
                  color: Color(0xFF333333),
                  child: kIsWeb
                      ? Image.network(
                          baseImageUrl + "${widget.movies.posterPath}",
                          width: double.infinity,
                          fit: BoxFit.cover)
                      : CachedNetworkImage(
                          width: double.infinity,
                          imageUrl:
                            baseImageUrl + "${widget.movies.posterPath}",
                          fit: BoxFit.cover,
                        ),
                ),
              ),
              Container(
                  height: MediaQuery.of(context).size.height * 0.5,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.bottomCenter,
                        end: Alignment.topCenter,
                        colors: [
                          Color(0xFF000000).withOpacity(1),
                          Colors.transparent,
                        ],
                        stops: [
                          0.2,
                          0.4,
                        ]),
                  )),
              Positioned(
                top: 30,
                left: 16,
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Container(
                    margin: EdgeInsets.only(top: 16),
                    height: 45,
                    width: 45,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: Colors.white24),
                    child: Icon(
                      Icons.arrow_back,
                      color: Colors.white,
                      size: 30,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(
                  widget.movies.title,
                  style: TextStyle(
                      fontSize: MediaQuery.of(context).size.width * 0.08,
                      color: Color(0xFFFBFBFB)
                      ),
                ),
              ),
            ],
          ),
          Expanded(
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        genresView(),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10.0),
                          child: Text("🎬  電影簡介",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: MediaQuery.of(context).size.width * 0.06)),
                        ),
                        Text(widget.movies.overview,
                            style: TextStyle(
                                color: Colors.white.withOpacity(.8),
                                fontSize: MediaQuery.of(context).size.width * 0.04)),
                        Padding(
                          padding: const EdgeInsets.only(top: 20.0, bottom: 10.0),
                          child: Text("🎞️  相關影片",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: MediaQuery.of(context).size.width * 0.06)),
                        ),
                        Container(
                          alignment: Alignment.center,
                          height: 240,
                          width: MediaQuery.of(context).size.width,
                          child: SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: 
                              BlocBuilder<VideoBloc, VideoState>(builder: (context, state) {
                                if (state is VideoInitialState) {
                                  return CircularProgressIndicator();
                                } else if (state is VideoLoadingState) {
                                  return CircularProgressIndicator();
                                } else if (state is VideoLoadedState) {
                                  print(state.videos);
                                  return VideosView(state.videos);
                                } else if (state is VideoErrorState) {
                                  return Center(child: EmptyItemView());
                                }
                                return Center(child: EmptyItemView());
                              })
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget genresView() {
    final List<String> geners = getMovieGenres(widget.movies.genreIds);

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: movieGenersListDetail(context, geners),
    );
  }
}

Widget movieGenersListDetail(context, List<String> strings) {
  List<Widget> list = new List<Widget>();
  for (var i = 0; i < strings.length; i++) {
    list.add(Container(
      decoration: BoxDecoration(
          color: Color(0xFFee6969), borderRadius: BorderRadius.circular(5)),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 13),
        child: new Text(
          strings[i],
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w500,
            letterSpacing: 0.2,
            fontSize: MediaQuery.of(context).size.width * 0.04,
          )
        ),
      ),
    ));
  }
  return new Wrap(runSpacing: 4.0, spacing: 10.0, children: list);
}

class VideosView extends StatelessWidget {
  final List<VideoItem> videos;
  VideosView(this.videos);

  @override
  Widget build(BuildContext context) {
    if (videos == null || videos?.length == 0)
      return EmptyItemView();
    
    return ListView.separated(
        separatorBuilder: (context, i) {
          return SizedBox(
            width: 10,
          );
        },
        scrollDirection: Axis.horizontal,
        itemCount: videos.length,
        shrinkWrap: true,
        itemBuilder: (context, i) {
          return VideoItemLayout(videos[i]);
        }
      );
  }
}

class VideoItemLayout extends StatelessWidget {
  final VideoItem item;
  VideoItemLayout(this.item);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.all(5.0),
          width: MediaQuery.of(context).size.width * 0.4,
          height: MediaQuery.of(context).size.width * 0.4 * 0.75,
          color: Colors.grey,
          child: Center(
              child: IconButton(icon: Icon(Icons.play_circle_filled),
                  onPressed: () {
                    openVideo(item.key, item.site);
                  },
              )
          ),
        ),
        Container(              
          width: MediaQuery.of(context).size.width * 0.4,
          child: Text(
            item.name,
            style: TextStyle(
              fontSize: 14,
              color: Color(0xFFFBFBFB)
            ),
          maxLines: 2,
          overflow: TextOverflow.fade,
          )
        ),
      ],
    );
  }
}

Future openVideo(String key, String site) async {
   String videoBaseUrl = '';
    if(site == 'YouTube') {
      videoBaseUrl = youtubeVideoUrl;
    } else {
      videoBaseUrl = vimeoVideoUrl;
    }

    String url = videoBaseUrl + key;
    if(await canLaunch(url)) {
      await launch(url);
    } else {
      throw Exception;
    }
}