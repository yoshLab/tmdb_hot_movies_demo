import 'package:flutter/material.dart';

class DefaultErrorView extends StatelessWidget {
  final String title;
  final String message;

  DefaultErrorView({this.title = "Oops!", this.message = "目前無法顯示畫面，請稍後再試"});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(
                fontSize: 32, color: Colors.blueGrey),
          ),
          SizedBox(height: 10.0),
          Text(
            message,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 16, color: Colors.grey),
          ),
        ],
      ),
    );
  }
}

class EmptyItemView extends DefaultErrorView {
  EmptyItemView({String emptyTitle = "Oops!", String emptyMessage = "查無資料"}) : super(title: emptyTitle, message: emptyMessage);
}
