const String api_key = "069b86182c7f5a543f258f68c6ce264e";
const String baseUrl = "http://api.themoviedb.org/3/";
const String baseImageUrl = "https://image.tmdb.org/t/p/w1280";
const String youtubeVideoUrl = "https://www.youtube.com/watch?v=";
const String vimeoVideoUrl = "https://vimeo.com/";
const String launchTitle = "TMDb";

const Map<int, String> genreName = {
  28: '動作',
  12: '冒險',
  16: '動畫',
  35: '喜劇',
  80: '犯罪',
  99: '紀錄片',
  18: '劇情',
  10751: '家庭',
  10762: "兒童",
  14: '奇幻',
  36: '歷史',
  27: '恐怖',
  10402: '音樂劇',
  9648: '推理',
  10749: '浪漫',
  878: '科幻',
  10770: '電視劇',
  53: '驚悚',
  10752: '戰爭',
  37: '西方'
};

List<String> getMovieGenres(List<int> genres) {
  List<String> genreList = [];

  genres.forEach((genre) {
    var genreTest = genreName[genre];
    if (genreTest != null) {
        genreList.add(genreName[genre]);
    }
  });

  return genreList;
}

const Map<String, String> MovieCategory = {
  "now_playing" : "現正熱映",
  "upcoming" : "即將上映",
  "popular" : "熱門電影",
  "top_rated" : "評分最高",
  // "latest" : "最新電影",
};