import 'package:tmdb_hot_movies_demo/Model/api_video_model.dart';
import 'package:tmdb_hot_movies_demo/Utils/key.dart';
import 'package:tmdb_hot_movies_demo/Model/api_result_model.dart';
import 'package:http/http.dart' as http;

import 'dart:convert';

abstract class MovieRepository {
  Future<List<Results>> getMovies(String movieType);
  Future<List<Results>> getMoviesBySearch(String query);
  Future<List<VideoItem>> getVideos(String movieId);
}

class MovieRepositoryImpl implements MovieRepository {
  @override
  Future<List<Results>> getMovies(String movieType) async {
    var response = await http.get(baseUrl + "movie/$movieType?api_key=$api_key&language=zh-TW");

    if (response.statusCode == 200) {
      var data = json.decode(response.body);

      List<Results> movies = ApiResultModel.fromJson(data).results;
      return movies;
    } 
    else {
      throw Exception();
    }
  }

  Future<List<Results>> getMoviesBySearch(String keyword) async {
    var response =
        await http.get(baseUrl + "search/movie?api_key=$api_key&query=$keyword&language=zh-TW");

    if (response.statusCode == 200) {
      var data = json.decode(response.body);

      List<Results> movies = ApiResultModel.fromJson(data).results;
      return movies;
    } 
    else {
      throw Exception();
    }
  }

  Future<List<VideoItem>> getVideos(String movieId) async {
    var response =
        await http.get(baseUrl + "movie/$movieId/videos?api_key=$api_key&language=en-US");

    if (response.statusCode == 200) {
      var data = json.decode(response.body);

      List<VideoItem> movies = VideoResults.fromJson(data).results;
      return movies;
    } 
    else {
      throw Exception();
    }
  }
}
