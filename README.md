# tmdb_hot_movies_demo
> tmdb_hot_movies_demo 是一款使用 Flutter Framework 開發的跨平台 Demo App，利用 [TMDb](https://www.themoviedb.org) 提供的 API 抓取電影相關資訊。程式架構採用 BLoC pattern，實現 UI 與商業邏輯的分離。此 Demo App 僅供學習與交流使用。

### 開發環境

- VS Code
- Dart

### 功能介紹

-  [x] 以 Tab 呈現多個電影熱門類別
-  [x] 電影簡介與相關影片播放
-  [x] 關鍵字搜尋功能
-  [x] 已載入的圖片使用 `cached_network_image` 緩存
-  [x] 頁面載入與切換時的動畫效果
-  [ ] Bottom Tab 切換電影和電影明星列表
-  [ ] 電影分享功能

### TMDb API Documentation

- 院線查詢： [Movie now_playing API](https://developers.themoviedb.org/3/movies/get-now-playing)
- 電影關鍵字查詢： [Movie Search API](https://developers.themoviedb.org/3/search/search-movies)
- 相關影片查詢： [Get Videos API](https://developers.themoviedb.org/3/movies/get-movie-videos)

* 注意：以上皆須自行產生 api_key

### 👤 聯絡方式
yosh - joey.yu.tw@gmail.com

[https://gitlab.com/yoshLab/tmdb_hot_movies_demo](https://gitlab.com/yoshLab/tmdb_hot_movies_demo)